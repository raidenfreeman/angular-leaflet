'use strict';

angular.module('myApp.view1', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
      $routeProvider.when('/view1', {
        templateUrl: 'view1/view1.html',
        controller: 'View1Ctrl'
      });
    }])

    .controller('View1Ctrl', [ '$scope', function($scope) {
      var start = {x:0.,y:0};
      var end = {x:12,y:3};
      var step = 0.75;
      $scope.startingPoint = start;
      $scope.destinationPoint = end;
      $scope.lerpedValue = lerp(start,end,step);
    }]);

function lerp(vector2Origin, vector2Target, step)
{
  if(step== undefined)
    step = 0;
  if(step>1 || step<0)
    throw "lerp step must be between 0 and 1";
  return { x : vector2Origin.x * (1.0 - step) + vector2Target.x * step , y : vector2Origin.y * (1.0 - step) + vector2Target.y * step };
}

function moveSmoothly(vector2Origin, vector2Target, timeInSeconds)
{

}