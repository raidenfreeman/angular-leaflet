'use strict';

angular.module('myApp.MapView', ['leaflet-directive', 'ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/MapView', {
            templateUrl: 'MapView.html',
            controller: 'DemoController'
        });
    }])

    .controller('DemoController', [ '$scope', 'leafletData', function($scope, leafletData) {
        $scope.center = {
            lat: 51.005,
            lng: 0,
            zoom: 10
        };
        $scope.layers = {
            baselayers: {
                mapbox_terrain: {
                    name: 'Mapbox Terrain',
                    url: 'http://api.tiles.mapbox.com/v4/{mapid}/{z}/{x}/{y}.png?access_token={apikey}',
                    type: 'xyz',
                    layerOptions: {
                        apikey: 'pk.eyJ1IjoicmFpZGVuZnJlZW1hbiIsImEiOiJjaWZkcXZ1eTgwMGQ4dG5seWF2dzliZmhpIn0.BqZRSI9qyOANkq11vk0pGw',
                        mapid: 'raidenfreeman.cifdqvul400d4telxe5joz0vb'
                    }
                },
                osm: {
                    name: 'OpenStreetMap',
                    url: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                    type: 'xyz'
                }
            }
        };
        $scope.paths = {
            example: {
                type: "polyline",
                latlngs: [ { lat: 51, lng: -0.09 }, { lat: 51.005, lng: 0 }, { lat: 51.02, lng: 0.05 } ],
                color: "red"
            }
        };
    }]);
